# Examples (10,0.75)

50 graphs, with 10 nodes each. The probability of any edge existing is 0.75.


## Graph 0

```
0,1,1,0,1,1,1,1,0,1
1,0,1,0,0,1,1,0,1,1
1,1,0,1,0,1,1,1,1,0
0,0,1,0,1,1,0,1,0,0
1,0,0,1,0,1,1,0,1,1
1,1,1,1,1,0,0,1,1,1
1,1,1,0,1,0,0,0,1,0
1,0,1,1,0,1,0,0,1,0
0,1,1,0,1,1,1,1,0,0
1,1,0,0,1,1,0,0,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00000_04_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00000_04_dot.png "drawn by dot")

## Graph 1

```
0,1,1,0,1,1,0,0,1,1
1,0,1,1,0,1,0,1,0,1
1,1,0,1,1,1,1,1,0,1
0,1,1,0,1,1,1,0,0,1
1,0,1,1,0,1,0,1,1,1
1,1,1,1,1,0,1,0,1,0
0,0,1,1,0,1,0,1,1,1
0,1,1,0,1,0,1,0,1,1
1,0,0,0,1,1,1,1,0,1
1,1,1,1,1,0,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00001_04_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00001_04_dot.png "drawn by dot")

## Graph 2

```
0,0,0,1,1,1,1,1,1,1
0,0,1,1,1,1,0,0,0,1
0,1,0,0,1,1,1,1,0,0
1,1,0,0,1,0,1,1,1,0
1,1,1,1,0,0,0,1,1,1
1,1,1,0,0,0,1,0,1,0
1,0,1,1,0,1,0,0,0,1
1,0,1,1,1,0,0,0,1,0
1,0,0,1,1,1,0,1,0,1
1,1,0,0,1,0,1,0,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00002_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00002_05_dot.png "drawn by dot")

## Graph 3

```
0,1,1,0,1,1,0,1,1,0
1,0,1,1,1,0,1,0,1,1
1,1,0,1,1,0,1,1,1,1
0,1,1,0,1,0,1,1,1,1
1,1,1,1,0,0,0,1,0,0
1,0,0,0,0,0,0,1,1,1
0,1,1,1,0,0,0,0,0,0
1,0,1,1,1,1,0,0,0,1
1,1,1,1,0,1,0,0,0,1
0,1,1,1,0,1,0,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00003_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00003_05_dot.png "drawn by dot")

## Graph 4

```
0,1,1,0,1,1,1,0,1,0
1,0,1,0,1,1,1,0,0,1
1,1,0,1,0,1,1,1,0,0
0,0,1,0,1,1,1,1,0,0
1,1,0,1,0,0,0,1,1,0
1,1,1,1,0,0,1,1,1,1
1,1,1,1,0,1,0,1,1,1
0,0,1,1,1,1,1,0,0,1
1,0,0,0,1,1,1,0,0,1
0,1,0,0,0,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00004_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00004_05_dot.png "drawn by dot")

## Graph 5

```
0,1,1,1,1,0,1,1,0,0
1,0,1,1,1,1,1,1,0,0
1,1,0,0,0,1,1,0,1,1
1,1,0,0,1,1,1,1,0,0
1,1,0,1,0,1,1,0,1,0
0,1,1,1,1,0,1,1,0,1
1,1,1,1,1,1,0,1,0,1
1,1,0,1,0,1,1,0,0,1
0,0,1,0,1,0,0,0,0,1
0,0,1,0,0,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00005_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00005_05_dot.png "drawn by dot")

## Graph 6

```
0,1,0,1,0,0,1,0,0,0
1,0,0,1,1,0,1,0,1,1
0,0,0,1,1,1,1,1,1,1
1,1,1,0,0,1,1,1,0,1
0,1,1,0,0,1,1,1,1,1
0,0,1,1,1,0,1,1,0,0
1,1,1,1,1,1,0,0,1,1
0,0,1,1,1,1,0,0,1,1
0,1,1,0,1,0,1,1,0,1
0,1,1,1,1,0,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00006_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00006_05_dot.png "drawn by dot")

## Graph 7

```
0,1,1,1,1,0,1,1,1,0
1,0,0,1,1,1,0,1,1,0
1,0,0,1,1,1,1,0,1,1
1,1,1,0,0,1,0,0,1,1
1,1,1,0,0,1,1,1,0,1
0,1,1,1,1,0,0,1,0,1
1,0,1,0,1,0,0,1,0,1
1,1,0,0,1,1,1,0,1,1
1,1,1,1,0,0,0,1,0,0
0,0,1,1,1,1,1,1,0,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00007_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00007_05_dot.png "drawn by dot")

## Graph 8

```
0,1,1,1,0,1,0,0,1,1
1,0,0,1,1,1,0,1,0,0
1,0,0,1,0,1,0,1,1,1
1,1,1,0,1,1,1,1,0,1
0,1,0,1,0,0,1,1,1,1
1,1,1,1,0,0,0,1,1,0
0,0,0,1,1,0,0,1,1,1
0,1,1,1,1,1,1,0,1,0
1,0,1,0,1,1,1,1,0,1
1,0,1,1,1,0,1,0,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00008_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00008_05_dot.png "drawn by dot")

## Graph 9

```
0,0,1,1,1,1,1,0,1,1
0,0,1,1,1,1,1,1,1,0
1,1,0,0,0,1,1,1,0,1
1,1,0,0,1,0,1,1,0,0
1,1,0,1,0,0,1,0,1,1
1,1,1,0,0,0,0,1,1,0
1,1,1,1,1,0,0,1,1,1
0,1,1,1,0,1,1,0,1,1
1,1,0,0,1,1,1,1,0,1
1,0,1,0,1,0,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00009_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00009_05_dot.png "drawn by dot")

## Graph 10

```
0,1,1,1,1,1,1,0,1,1
1,0,1,1,0,1,0,1,1,1
1,1,0,1,1,0,1,1,1,1
1,1,1,0,1,1,0,1,1,0
1,0,1,1,0,1,0,0,0,1
1,1,0,1,1,0,1,1,0,1
1,0,1,0,0,1,0,0,1,0
0,1,1,1,0,1,0,0,0,1
1,1,1,1,0,0,1,0,0,1
1,1,1,0,1,1,0,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00010_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00010_05_dot.png "drawn by dot")

## Graph 11

```
0,1,1,1,1,1,0,1,0,1
1,0,1,0,1,0,0,1,0,1
1,1,0,0,1,0,0,0,1,1
1,0,0,0,1,1,1,0,1,0
1,1,1,1,0,1,1,1,1,1
1,0,0,1,1,0,1,1,1,1
0,0,0,1,1,1,0,1,1,0
1,1,0,0,1,1,1,0,1,1
0,0,1,1,1,1,1,1,0,1
1,1,1,0,1,1,0,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00011_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00011_05_dot.png "drawn by dot")

## Graph 12

```
0,0,0,1,1,1,0,1,1,0
0,0,1,0,0,1,1,1,1,1
0,1,0,1,1,0,0,0,0,1
1,0,1,0,1,1,1,1,1,1
1,0,1,1,0,1,1,1,1,1
1,1,0,1,1,0,1,0,1,1
0,1,0,1,1,1,0,1,0,1
1,1,0,1,1,0,1,0,1,0
1,1,0,1,1,1,0,1,0,1
0,1,1,1,1,1,1,0,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00012_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00012_05_dot.png "drawn by dot")

## Graph 13

```
0,1,1,1,0,0,1,1,1,0
1,0,1,1,1,0,1,1,1,1
1,1,0,1,1,0,1,0,1,1
1,1,1,0,0,0,1,1,0,0
0,1,1,0,0,1,1,1,0,1
0,0,0,0,1,0,1,1,0,1
1,1,1,1,1,1,0,1,1,1
1,1,0,1,1,1,1,0,0,1
1,1,1,0,0,0,1,0,0,1
0,1,1,0,1,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00013_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00013_05_dot.png "drawn by dot")

## Graph 14

```
0,1,0,1,1,1,1,0,1,1
1,0,1,1,1,0,1,1,0,0
0,1,0,0,1,1,0,1,1,1
1,1,0,0,1,1,1,0,1,0
1,1,1,1,0,1,1,1,0,1
1,0,1,1,1,0,1,1,1,0
1,1,0,1,1,1,0,0,1,1
0,1,1,0,1,1,0,0,1,1
1,0,1,1,0,1,1,1,0,1
1,0,1,0,1,0,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00014_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00014_05_dot.png "drawn by dot")

## Graph 15

```
0,0,1,1,0,1,0,1,1,1
0,0,1,1,0,1,1,1,1,1
1,1,0,1,1,1,1,1,1,1
1,1,1,0,0,1,0,1,1,0
0,0,1,0,0,1,0,1,1,1
1,1,1,1,1,0,0,1,1,0
0,1,1,0,0,0,0,0,1,1
1,1,1,1,1,1,0,0,0,1
1,1,1,1,1,1,1,0,0,1
1,1,1,0,1,0,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00015_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00015_05_dot.png "drawn by dot")

## Graph 16

```
0,1,1,1,1,1,0,0,0,1
1,0,1,1,1,0,0,1,1,0
1,1,0,1,1,1,1,1,1,1
1,1,1,0,1,0,1,0,1,1
1,1,1,1,0,1,1,1,0,1
1,0,1,0,1,0,1,1,1,0
0,0,1,1,1,1,0,1,1,1
0,1,1,0,1,1,1,0,1,1
0,1,1,1,0,1,1,1,0,1
1,0,1,1,1,0,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00016_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00016_05_dot.png "drawn by dot")

## Graph 17

```
0,0,1,1,0,1,1,1,1,1
0,0,0,1,1,1,1,0,1,1
1,0,0,1,1,1,1,0,0,0
1,1,1,0,1,1,1,1,0,1
0,1,1,1,0,1,1,0,1,1
1,1,1,1,1,0,0,1,1,1
1,1,1,1,1,0,0,1,1,1
1,0,0,1,0,1,1,0,1,1
1,1,0,0,1,1,1,1,0,1
1,1,0,1,1,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00017_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00017_05_dot.png "drawn by dot")

## Graph 18

```
0,1,1,0,1,0,1,1,1,1
1,0,1,0,1,1,0,1,1,1
1,1,0,0,1,1,0,0,1,1
0,0,0,0,0,1,1,1,1,1
1,1,1,0,0,1,1,0,1,1
0,1,1,1,1,0,1,1,1,1
1,0,0,1,1,1,0,1,1,1
1,1,0,1,0,1,1,0,1,1
1,1,1,1,1,1,1,1,0,0
1,1,1,1,1,1,1,1,0,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00018_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00018_05_dot.png "drawn by dot")

## Graph 19

```
0,1,1,0,1,1,1,0,1,1
1,0,1,1,1,1,1,1,0,1
1,1,0,0,1,1,1,1,0,1
0,1,0,0,1,1,1,0,1,1
1,1,1,1,0,0,1,1,1,0
1,1,1,1,0,0,1,1,1,0
1,1,1,1,1,1,0,1,1,1
0,1,1,0,1,1,1,0,1,1
1,0,0,1,1,1,1,1,0,1
1,1,1,1,0,0,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00019_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00019_05_dot.png "drawn by dot")

## Graph 20

```
0,1,1,0,1,1,1,1,1,1
1,0,1,1,1,1,1,1,1,0
1,1,0,1,1,0,1,1,1,1
0,1,1,0,1,0,1,1,1,1
1,1,1,1,0,1,0,1,0,1
1,1,0,0,1,0,1,1,1,0
1,1,1,1,0,1,0,0,1,1
1,1,1,1,1,1,0,0,1,1
1,1,1,1,0,1,1,1,0,1
1,0,1,1,1,0,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00020_05_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00020_05_dot.png "drawn by dot")

## Graph 21

```
0,1,0,1,0,1,0,1,1,0
1,0,1,1,1,1,0,1,1,1
0,1,0,1,1,1,1,1,1,0
1,1,1,0,1,1,0,1,1,1
0,1,1,1,0,0,0,0,0,0
1,1,1,1,0,0,1,1,1,1
0,0,1,0,0,1,0,1,0,1
1,1,1,1,0,1,1,0,1,0
1,1,1,1,0,1,0,1,0,1
0,1,0,1,0,1,1,0,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00021_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00021_06_dot.png "drawn by dot")

## Graph 22

```
0,1,1,0,1,1,1,0,1,0
1,0,1,1,0,1,1,0,0,1
1,1,0,1,0,1,1,1,1,1
0,1,1,0,1,1,1,1,1,1
1,0,0,1,0,0,1,1,0,0
1,1,1,1,0,0,1,0,0,1
1,1,1,1,1,1,0,1,1,1
0,0,1,1,1,0,1,0,0,1
1,0,1,1,0,0,1,0,0,1
0,1,1,1,0,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00022_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00022_06_dot.png "drawn by dot")

## Graph 23

```
0,1,0,0,1,1,1,1,1,1
1,0,0,1,1,1,1,1,1,0
0,0,0,1,1,1,1,1,0,1
0,1,1,0,0,1,1,0,0,1
1,1,1,0,0,1,1,1,1,1
1,1,1,1,1,0,1,0,1,1
1,1,1,1,1,1,0,0,1,1
1,1,1,0,1,0,0,0,0,1
1,1,0,0,1,1,1,0,0,0
1,0,1,1,1,1,1,1,0,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00023_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00023_06_dot.png "drawn by dot")

## Graph 24

```
0,1,0,1,0,1,1,1,1,0
1,0,1,1,1,1,1,1,0,0
0,1,0,1,1,1,0,1,1,1
1,1,1,0,1,1,1,1,0,0
0,1,1,1,0,0,0,1,1,1
1,1,1,1,0,0,1,1,1,1
1,1,0,1,0,1,0,1,0,1
1,1,1,1,1,1,1,0,1,1
1,0,1,0,1,1,0,1,0,0
0,0,1,0,1,1,1,1,0,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00024_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00024_06_dot.png "drawn by dot")

## Graph 25

```
0,1,1,1,1,1,1,0,1,1
1,0,0,1,1,1,1,1,1,1
1,0,0,0,1,0,1,1,0,1
1,1,0,0,0,0,1,0,1,0
1,1,1,0,0,1,1,1,0,1
1,1,0,0,1,0,1,0,1,1
1,1,1,1,1,1,0,1,1,1
0,1,1,0,1,0,1,0,1,1
1,1,0,1,0,1,1,1,0,0
1,1,1,0,1,1,1,1,0,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00025_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00025_06_dot.png "drawn by dot")

## Graph 26

```
0,0,1,1,1,0,0,1,1,1
0,0,0,1,1,1,1,1,1,1
1,0,0,1,1,0,1,0,1,0
1,1,1,0,0,1,1,1,1,1
1,1,1,0,0,0,1,1,1,0
0,1,0,1,0,0,0,1,1,1
0,1,1,1,1,0,0,1,1,1
1,1,0,1,1,1,1,0,1,1
1,1,1,1,1,1,1,1,0,1
1,1,0,1,0,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00026_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00026_06_dot.png "drawn by dot")

## Graph 27

```
0,1,1,1,1,1,0,1,1,1
1,0,0,0,1,1,1,1,1,0
1,0,0,0,1,0,0,1,1,1
1,0,0,0,0,1,1,1,1,0
1,1,1,0,0,1,1,1,1,0
1,1,0,1,1,0,1,1,1,1
0,1,0,1,1,1,0,0,1,1
1,1,1,1,1,1,0,0,1,1
1,1,1,1,1,1,1,1,0,1
1,0,1,0,0,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00027_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00027_06_dot.png "drawn by dot")

## Graph 28

```
0,1,1,0,0,1,0,0,0,1
1,0,1,1,1,1,1,0,1,1
1,1,0,1,0,1,1,0,1,1
0,1,1,0,1,1,1,1,1,1
0,1,0,1,0,1,1,1,1,0
1,1,1,1,1,0,1,1,1,1
0,1,1,1,1,1,0,0,1,0
0,0,0,1,1,1,0,0,1,1
0,1,1,1,1,1,1,1,0,1
1,1,1,1,0,1,0,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00028_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00028_06_dot.png "drawn by dot")

## Graph 29

```
0,1,0,1,1,1,1,0,1,1
1,0,1,0,1,1,0,1,1,0
0,1,0,1,1,0,1,1,1,1
1,0,1,0,1,0,1,1,1,1
1,1,1,1,0,0,1,0,1,1
1,1,0,0,0,0,1,1,0,1
1,0,1,1,1,1,0,1,1,1
0,1,1,1,0,1,1,0,1,1
1,1,1,1,1,0,1,1,0,1
1,0,1,1,1,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00029_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00029_06_dot.png "drawn by dot")

## Graph 30

```
0,0,1,0,0,1,1,1,0,1
0,0,1,1,1,1,0,1,0,1
1,1,0,1,1,0,1,1,1,0
0,1,1,0,1,1,1,1,1,1
0,1,1,1,0,1,1,1,1,1
1,1,0,1,1,0,1,1,1,1
1,0,1,1,1,1,0,0,1,0
1,1,1,1,1,1,0,0,1,1
0,0,1,1,1,1,1,1,0,1
1,1,0,1,1,1,0,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00030_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00030_06_dot.png "drawn by dot")

## Graph 31

```
0,0,1,1,1,1,1,1,1,1
0,0,1,1,1,1,1,1,1,1
1,1,0,1,1,0,1,0,0,1
1,1,1,0,1,1,1,1,0,1
1,1,1,1,0,0,1,0,0,0
1,1,0,1,0,0,1,1,1,1
1,1,1,1,1,1,0,1,1,1
1,1,0,1,0,1,1,0,0,1
1,1,0,0,0,1,1,0,0,1
1,1,1,1,0,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00031_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00031_06_dot.png "drawn by dot")

## Graph 32

```
0,0,1,1,1,1,1,0,1,1
0,0,1,0,0,1,1,1,1,1
1,1,0,1,1,0,1,1,1,1
1,0,1,0,1,1,1,1,1,1
1,0,1,1,0,1,0,1,1,1
1,1,0,1,1,0,1,0,1,1
1,1,1,1,0,1,0,1,0,0
0,1,1,1,1,0,1,0,1,0
1,1,1,1,1,1,0,1,0,1
1,1,1,1,1,1,0,0,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00032_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00032_06_dot.png "drawn by dot")

## Graph 33

```
0,1,1,1,1,1,1,1,1,1
1,0,0,1,1,1,1,0,1,0
1,0,0,1,1,0,0,1,1,1
1,1,1,0,1,1,0,1,0,1
1,1,1,1,0,1,1,1,0,1
1,1,0,1,1,0,0,1,1,1
1,1,0,0,1,0,0,1,0,1
1,0,1,1,1,1,1,0,1,1
1,1,1,0,0,1,0,1,0,1
1,0,1,1,1,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00033_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00033_06_dot.png "drawn by dot")

## Graph 34

```
0,1,1,1,0,1,1,0,1,0
1,0,1,1,1,1,1,0,1,1
1,1,0,1,1,1,1,1,1,0
1,1,1,0,0,1,1,1,1,1
0,1,1,0,0,1,1,1,1,1
1,1,1,1,1,0,1,0,0,1
1,1,1,1,1,1,0,1,0,1
0,0,1,1,1,0,1,0,0,1
1,1,1,1,1,0,0,0,0,1
0,1,0,1,1,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00034_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00034_06_dot.png "drawn by dot")

## Graph 35

```
0,1,1,1,0,0,1,1,0,1
1,0,1,1,0,1,1,0,1,1
1,1,0,1,1,1,1,1,1,1
1,1,1,0,1,0,1,0,1,1
0,0,1,1,0,0,0,1,1,0
0,1,1,0,0,0,1,1,1,1
1,1,1,1,0,1,0,1,1,1
1,0,1,0,1,1,1,0,1,1
0,1,1,1,1,1,1,1,0,1
1,1,1,1,0,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00035_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00035_06_dot.png "drawn by dot")

## Graph 36

```
0,1,1,1,1,1,1,1,1,0
1,0,1,0,0,1,1,1,1,0
1,1,0,0,1,1,1,1,1,1
1,0,0,0,0,1,1,1,1,0
1,0,1,0,0,0,1,1,0,1
1,1,1,1,0,0,1,1,1,1
1,1,1,1,1,1,0,1,1,1
1,1,1,1,1,1,1,0,0,1
1,1,1,1,0,1,1,0,0,1
0,0,1,0,1,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00036_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00036_06_dot.png "drawn by dot")

## Graph 37

```
0,1,1,0,1,1,1,1,1,1
1,0,1,1,1,1,1,0,0,0
1,1,0,1,1,1,1,0,1,1
0,1,1,0,1,1,1,1,1,0
1,1,1,1,0,0,1,1,1,1
1,1,1,1,0,0,1,1,1,0
1,1,1,1,1,1,0,1,1,1
1,0,0,1,1,1,1,0,0,1
1,0,1,1,1,1,1,0,0,1
1,0,1,0,1,0,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00037_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00037_06_dot.png "drawn by dot")

## Graph 38

```
0,0,1,1,0,1,1,1,0,1
0,0,1,0,0,1,1,1,1,1
1,1,0,1,1,1,1,1,1,1
1,0,1,0,1,1,1,1,1,1
0,0,1,1,0,1,1,0,1,1
1,1,1,1,1,0,1,1,1,0
1,1,1,1,1,1,0,0,1,1
1,1,1,1,0,1,0,0,1,1
0,1,1,1,1,1,1,1,0,0
1,1,1,1,1,0,1,1,0,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00038_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00038_06_dot.png "drawn by dot")

## Graph 39

```
0,1,1,1,1,1,1,0,1,1
1,0,1,1,1,0,0,0,1,0
1,1,0,1,0,1,1,1,1,1
1,1,1,0,1,1,1,0,1,1
1,1,0,1,0,1,1,1,1,1
1,0,1,1,1,0,1,1,0,1
1,0,1,1,1,1,0,1,0,1
0,0,1,0,1,1,1,0,1,1
1,1,1,1,1,0,0,1,0,1
1,0,1,1,1,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00039_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00039_06_dot.png "drawn by dot")

## Graph 40

```
0,0,0,1,1,0,1,1,1,1
0,0,1,1,1,1,1,1,0,1
0,1,0,1,0,1,1,1,1,1
1,1,1,0,1,1,1,1,1,1
1,1,0,1,0,1,0,1,0,1
0,1,1,1,1,0,1,1,0,1
1,1,1,1,0,1,0,1,1,1
1,1,1,1,1,1,1,0,1,0
1,0,1,1,0,0,1,1,0,1
1,1,1,1,1,1,1,0,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00040_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00040_06_dot.png "drawn by dot")

## Graph 41

```
0,0,1,1,1,1,1,1,1,1
0,0,1,1,0,1,1,1,1,1
1,1,0,1,1,1,1,1,1,1
1,1,1,0,1,1,0,1,1,1
1,0,1,1,0,1,1,0,0,1
1,1,1,1,1,0,1,0,1,1
1,1,1,0,1,1,0,1,0,1
1,1,1,1,0,0,1,0,1,0
1,1,1,1,0,1,0,1,0,1
1,1,1,1,1,1,1,0,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00041_06_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00041_06_dot.png "drawn by dot")

## Graph 42

```
0,1,1,1,1,1,1,1,1,1
1,0,0,0,0,0,1,0,0,1
1,0,0,1,1,1,1,1,1,1
1,0,1,0,1,1,1,1,0,1
1,0,1,1,0,1,0,1,0,1
1,0,1,1,1,0,1,1,1,1
1,1,1,1,0,1,0,1,1,1
1,0,1,1,1,1,1,0,0,1
1,0,1,0,0,1,1,0,0,1
1,1,1,1,1,1,1,1,1,0
```

Its chromatic number is 7.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00042_07_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00042_07_dot.png "drawn by dot")

## Graph 43

```
0,1,1,1,1,1,0,1,1,0
1,0,1,1,1,1,0,1,1,0
1,1,0,1,1,1,1,1,1,1
1,1,1,0,1,1,1,1,1,1
1,1,1,1,0,0,0,1,1,1
1,1,1,1,0,0,0,1,1,1
0,0,1,1,0,0,0,1,1,1
1,1,1,1,1,1,1,0,1,0
1,1,1,1,1,1,1,1,0,1
0,0,1,1,1,1,1,0,1,0
```

Its chromatic number is 7.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00043_07_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00043_07_dot.png "drawn by dot")

## Graph 44

```
0,1,1,1,0,1,1,1,1,1
1,0,0,1,1,1,1,1,1,0
1,0,0,1,1,1,1,0,1,1
1,1,1,0,1,1,1,1,1,1
0,1,1,1,0,0,0,1,1,1
1,1,1,1,0,0,1,1,1,1
1,1,1,1,0,1,0,0,1,0
1,1,0,1,1,1,0,0,1,1
1,1,1,1,1,1,1,1,0,1
1,0,1,1,1,1,0,1,1,0
```

Its chromatic number is 7.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00044_07_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00044_07_dot.png "drawn by dot")

## Graph 45

```
0,0,1,0,1,1,1,1,0,1
0,0,1,1,1,1,1,1,0,1
1,1,0,1,1,1,1,1,1,1
0,1,1,0,0,1,1,1,1,1
1,1,1,0,0,1,0,1,1,1
1,1,1,1,1,0,1,1,1,1
1,1,1,1,0,1,0,1,1,1
1,1,1,1,1,1,1,0,1,1
0,0,1,1,1,1,1,1,0,0
1,1,1,1,1,1,1,1,0,0
```

Its chromatic number is 7.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00045_07_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00045_07_dot.png "drawn by dot")

## Graph 46

```
0,1,1,1,1,1,1,1,1,1
1,0,1,1,1,1,1,0,1,1
1,1,0,1,1,1,1,1,1,1
1,1,1,0,1,0,0,1,1,1
1,1,1,1,0,1,1,0,1,1
1,1,1,0,1,0,0,1,1,0
1,1,1,0,1,0,0,1,1,1
1,0,1,1,0,1,1,0,1,1
1,1,1,1,1,1,1,1,0,1
1,1,1,1,1,0,1,1,1,0
```

Its chromatic number is 7.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00046_07_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00046_07_dot.png "drawn by dot")

## Graph 47

```
0,1,1,1,1,1,1,1,1,1
1,0,0,1,1,1,1,1,1,1
1,0,0,1,1,1,1,1,1,1
1,1,1,0,1,0,1,1,1,1
1,1,1,1,0,0,1,1,1,1
1,1,1,0,0,0,1,1,1,1
1,1,1,1,1,1,0,1,1,1
1,1,1,1,1,1,1,0,1,0
1,1,1,1,1,1,1,1,0,0
1,1,1,1,1,1,1,0,0,0
```

Its chromatic number is 7.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00047_07_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00047_07_dot.png "drawn by dot")

## Graph 48

```
0,1,1,1,1,1,1,0,1,1
1,0,1,1,1,1,0,1,1,1
1,1,0,1,1,1,1,1,1,1
1,1,1,0,1,1,1,1,1,1
1,1,1,1,0,1,1,1,1,1
1,1,1,1,1,0,1,1,0,1
1,0,1,1,1,1,0,1,1,1
0,1,1,1,1,1,1,0,1,1
1,1,1,1,1,0,1,1,0,0
1,1,1,1,1,1,1,1,0,0
```

Its chromatic number is 7.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00048_07_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00048_07_dot.png "drawn by dot")

## Graph 49

```
0,1,1,1,1,0,1,1,1,1
1,0,1,1,1,0,1,1,0,1
1,1,0,1,1,1,1,1,1,1
1,1,1,0,1,1,1,1,0,1
1,1,1,1,0,1,1,1,1,1
0,0,1,1,1,0,0,1,1,0
1,1,1,1,1,0,0,1,0,1
1,1,1,1,1,1,1,0,1,1
1,0,1,0,1,1,0,1,0,0
1,1,1,1,1,0,1,1,0,0
```

Its chromatic number is 8.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_10,0.75_00049_08_circo.png "drawn by circo")

![drawn by dot](png/random_10,0.75_00049_08_dot.png "drawn by dot")

