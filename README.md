# Optimally colored graph examples with visualizations

### What is this about?

A graph is a network of elements called nodes or vertices. A graph is defined by its number of nodes and the structure of the connections between its nodes: if a pair of nodes is connected, we say that an edge exists between the two nodes, and that the nodes are adjacent.

Graph coloring is the problem of associating a color with each node so that no two adjacent nodes have the same color. An optimal coloring of a graph is such an assignment of a color to each node utilizing only the minimum possible number of different colors. The chromatic number of a graph is the number of different colors in an optimal coloring.

Finding optimal colorings for arbitrary graphs is a very difficult problem computationally, as is finding the chromatic number. Even approximating the chromatic number isn't much easier.

The fastest known algorithms for all these problems have run-times that depend exponentially on the number of vertices. These algorithms are nontrivial and most also use exponential amounts of space (computer memory).

### What is this?

This is a repository of examples of optimally (as long as my implementation is correct) colored graphs, visualized with `circo` and `dot` from the GraphViz suite of graph drawing programs.

Additionally, the `xdot` program (which is independent from the GraphViz project) could be fed any DOT descriptions of a color graph (stored in the `dot` directory in this repository) to provide some interactivity. There are also multiple GraphViz/DOT playgrounds on the Web.

### How are the examples structured?

The colored graphs are categorized by number of nodes, then sorted by chromatic number and by number of edges.

### Caveat

With a big chromatic number, some mutually distinct colors may appear visually identical at first glance.

### How was this generated?

With https://gitlab.com/nsajko/graph_coloring

## The examples

[Examples (7,0.75)](examples_random_07,0.75.md "some examples")

[Examples (10,0.75)](examples_random_10,0.75.md "some examples")

[Examples (16,0.75)](examples_random_16,0.75.md "some examples")

[Examples (20,0.75)](examples_random_20,0.75.md "some examples")

[Examples (25,0.75)](examples_random_25,0.75.md "some examples")

