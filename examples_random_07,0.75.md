# Examples (7,0.75)

50 graphs, with 7 nodes each. The probability of any edge existing is 0.75.


## Graph 0

```
0,0,1,0,1,0,1
0,0,1,1,0,0,1
1,1,0,1,0,1,1
0,1,1,0,1,0,0
1,0,0,1,0,1,1
0,0,1,0,1,0,1
1,1,1,0,1,1,0
```

Its chromatic number is 3.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00000_03_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00000_03_dot.png "drawn by dot")

## Graph 1

```
0,1,1,1,1,0,1
1,0,1,1,1,0,0
1,1,0,1,0,0,0
1,1,1,0,1,1,1
1,1,0,1,0,0,0
0,0,0,1,0,0,0
1,0,0,1,0,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00001_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00001_04_dot.png "drawn by dot")

## Graph 2

```
0,0,1,0,1,1,1
0,0,1,0,0,1,0
1,1,0,1,1,1,0
0,0,1,0,0,0,1
1,0,1,0,0,1,1
1,1,1,0,1,0,1
1,0,0,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00002_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00002_04_dot.png "drawn by dot")

## Graph 3

```
0,0,1,0,0,1,1
0,0,1,1,0,1,1
1,1,0,1,0,1,0
0,1,1,0,1,1,0
0,0,0,1,0,1,1
1,1,1,1,1,0,0
1,1,0,0,1,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00003_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00003_04_dot.png "drawn by dot")

## Graph 4

```
0,1,0,0,1,1,0
1,0,1,1,1,0,1
0,1,0,1,1,1,0
0,1,1,0,1,1,0
1,1,1,1,0,1,0
1,0,1,1,1,0,0
0,1,0,0,0,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00004_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00004_04_dot.png "drawn by dot")

## Graph 5

```
0,0,1,0,0,0,1
0,0,1,0,1,1,0
1,1,0,1,1,1,1
0,0,1,0,0,1,1
0,1,1,0,0,1,1
0,1,1,1,1,0,1
1,0,1,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00005_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00005_04_dot.png "drawn by dot")

## Graph 6

```
0,0,1,1,0,0,0
0,0,1,1,0,1,1
1,1,0,1,1,1,1
1,1,1,0,0,1,0
0,0,1,0,0,1,1
0,1,1,1,1,0,1
0,1,1,0,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00006_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00006_04_dot.png "drawn by dot")

## Graph 7

```
0,1,1,1,1,1,1
1,0,1,0,0,0,1
1,1,0,1,1,0,1
1,0,1,0,1,0,1
1,0,1,1,0,1,0
1,0,0,0,1,0,0
1,1,1,1,0,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00007_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00007_04_dot.png "drawn by dot")

## Graph 8

```
0,1,1,1,1,1,1
1,0,1,1,0,1,0
1,1,0,1,1,0,1
1,1,1,0,0,0,1
1,0,1,0,0,0,1
1,1,0,0,0,0,0
1,0,1,1,1,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00008_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00008_04_dot.png "drawn by dot")

## Graph 9

```
0,0,1,1,1,1,1
0,0,1,0,1,0,1
1,1,0,1,0,0,1
1,0,1,0,1,0,0
1,1,0,1,0,1,1
1,0,0,0,1,0,1
1,1,1,0,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00009_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00009_04_dot.png "drawn by dot")

## Graph 10

```
0,1,1,0,1,1,0
1,0,1,1,1,0,1
1,1,0,0,1,0,0
0,1,0,0,1,1,0
1,1,1,1,0,1,1
1,0,0,1,1,0,1
0,1,0,0,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00010_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00010_04_dot.png "drawn by dot")

## Graph 11

```
0,1,0,0,0,0,1
1,0,1,0,1,1,1
0,1,0,1,1,1,1
0,0,1,0,0,1,1
0,1,1,0,0,0,1
0,1,1,1,0,0,1
1,1,1,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00011_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00011_04_dot.png "drawn by dot")

## Graph 12

```
0,0,1,1,0,1,1
0,0,1,1,0,1,1
1,1,0,0,0,1,1
1,1,0,0,1,1,0
0,0,0,1,0,1,0
1,1,1,1,1,0,1
1,1,1,0,0,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00012_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00012_04_dot.png "drawn by dot")

## Graph 13

```
0,1,1,0,1,0,1
1,0,1,1,1,0,1
1,1,0,1,1,0,1
0,1,1,0,1,0,1
1,1,1,1,0,1,0
0,0,0,0,1,0,1
1,1,1,1,0,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00013_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00013_04_dot.png "drawn by dot")

## Graph 14

```
0,0,0,1,1,1,1
0,0,0,0,1,1,0
0,0,0,1,1,1,1
1,0,1,0,1,1,1
1,1,1,1,0,1,0
1,1,1,1,1,0,1
1,0,1,1,0,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00014_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00014_04_dot.png "drawn by dot")

## Graph 15

```
0,0,1,1,1,0,1
0,0,1,1,1,1,0
1,1,0,1,0,1,1
1,1,1,0,1,0,1
1,1,0,1,0,1,1
0,1,1,0,1,0,0
1,0,1,1,1,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00015_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00015_04_dot.png "drawn by dot")

## Graph 16

```
0,1,0,1,0,1,1
1,0,0,0,1,1,1
0,0,0,1,1,1,1
1,0,1,0,0,1,1
0,1,1,0,0,0,1
1,1,1,1,0,0,1
1,1,1,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00016_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00016_04_dot.png "drawn by dot")

## Graph 17

```
0,0,1,1,1,0,1
0,0,1,0,1,1,1
1,1,0,1,0,0,1
1,0,1,0,1,1,0
1,1,0,1,0,1,1
0,1,0,1,1,0,1
1,1,1,0,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00017_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00017_04_dot.png "drawn by dot")

## Graph 18

```
0,1,0,0,1,1,1
1,0,0,1,1,1,1
0,0,0,1,1,1,1
0,1,1,0,1,0,1
1,1,1,1,0,0,1
1,1,1,0,0,0,0
1,1,1,1,1,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00018_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00018_04_dot.png "drawn by dot")

## Graph 19

```
0,1,1,0,1,1,0
1,0,1,1,0,1,1
1,1,0,1,0,1,0
0,1,1,0,1,1,1
1,0,0,1,0,1,0
1,1,1,1,1,0,1
0,1,0,1,0,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00019_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00019_04_dot.png "drawn by dot")

## Graph 20

```
0,1,1,1,1,1,1
1,0,0,1,0,1,1
1,0,0,0,1,0,1
1,1,0,0,1,1,0
1,0,1,1,0,1,1
1,1,0,1,1,0,0
1,1,1,0,1,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00020_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00020_04_dot.png "drawn by dot")

## Graph 21

```
0,1,0,1,1,1,1
1,0,1,0,1,1,1
0,1,0,1,0,1,0
1,0,1,0,1,1,1
1,1,0,1,0,1,0
1,1,1,1,1,0,1
1,1,0,1,0,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00021_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00021_04_dot.png "drawn by dot")

## Graph 22

```
0,0,1,1,1,1,0
0,0,0,1,1,1,1
1,0,0,1,0,1,1
1,1,1,0,1,1,1
1,1,0,1,0,0,1
1,1,1,1,0,0,1
0,1,1,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00022_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00022_04_dot.png "drawn by dot")

## Graph 23

```
0,0,1,0,1,1,0
0,0,1,1,0,1,1
1,1,0,1,1,1,1
0,1,1,0,1,1,1
1,0,1,1,0,1,1
1,1,1,1,1,0,0
0,1,1,1,1,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00023_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00023_04_dot.png "drawn by dot")

## Graph 24

```
0,0,1,1,1,1,1
0,0,0,1,1,1,1
1,0,0,0,1,1,1
1,1,0,0,1,1,1
1,1,1,1,0,0,1
1,1,1,1,0,0,0
1,1,1,1,1,0,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00024_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00024_04_dot.png "drawn by dot")

## Graph 25

```
0,0,1,0,1,1,1
0,0,1,1,1,0,1
1,1,0,1,1,1,0
0,1,1,0,1,0,1
1,1,1,1,0,1,1
1,0,1,0,1,0,1
1,1,0,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00025_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00025_04_dot.png "drawn by dot")

## Graph 26

```
0,0,0,1,1,1,1
0,0,0,1,1,1,1
0,0,0,1,1,1,0
1,1,1,0,1,1,0
1,1,1,1,0,1,1
1,1,1,1,1,0,1
1,1,0,0,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00026_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00026_04_dot.png "drawn by dot")

## Graph 27

```
0,1,0,1,1,1,1
1,0,0,1,1,0,1
0,0,0,1,1,0,1
1,1,1,0,1,1,0
1,1,1,1,0,1,1
1,0,0,1,1,0,1
1,1,1,0,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00027_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00027_04_dot.png "drawn by dot")

## Graph 28

```
0,1,0,0,0,1,1
1,0,1,1,1,0,1
0,1,0,0,1,1,1
0,1,0,0,1,1,1
0,1,1,1,0,1,1
1,0,1,1,1,0,1
1,1,1,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00028_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00028_04_dot.png "drawn by dot")

## Graph 29

```
0,0,1,1,1,0,1
0,0,1,1,1,0,1
1,1,0,1,1,1,0
1,1,1,0,1,1,1
1,1,1,1,0,1,0
0,0,1,1,1,0,1
1,1,0,1,0,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00029_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00029_04_dot.png "drawn by dot")

## Graph 30

```
0,0,1,1,0,1,1
0,0,0,1,0,1,1
1,0,0,1,1,1,0
1,1,1,0,1,1,1
0,0,1,1,0,1,1
1,1,1,1,1,0,1
1,1,0,1,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00030_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00030_04_dot.png "drawn by dot")

## Graph 31

```
0,0,1,1,1,1,1
0,0,1,1,1,1,1
1,1,0,1,1,0,0
1,1,1,0,1,1,0
1,1,1,1,0,1,1
1,1,0,1,1,0,1
1,1,0,0,1,1,0
```

Its chromatic number is 4.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00031_04_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00031_04_dot.png "drawn by dot")

## Graph 32

```
0,1,0,1,1,1,1
1,0,1,1,0,1,1
0,1,0,0,1,0,0
1,1,0,0,0,1,1
1,0,1,0,0,1,0
1,1,0,1,1,0,1
1,1,0,1,0,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00032_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00032_05_dot.png "drawn by dot")

## Graph 33

```
0,1,1,0,1,1,1
1,0,0,0,1,1,1
1,0,0,0,1,1,0
0,0,0,0,1,0,0
1,1,1,1,0,1,1
1,1,1,0,1,0,1
1,1,0,0,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00033_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00033_05_dot.png "drawn by dot")

## Graph 34

```
0,1,1,1,1,0,1
1,0,0,1,0,0,0
1,0,0,1,1,1,1
1,1,1,0,1,1,1
1,0,1,1,0,0,1
0,0,1,1,0,0,1
1,0,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00034_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00034_05_dot.png "drawn by dot")

## Graph 35

```
0,0,1,1,0,1,1
0,0,1,1,1,1,1
1,1,0,1,1,1,1
1,1,1,0,0,1,1
0,1,1,0,0,0,0
1,1,1,1,0,0,1
1,1,1,1,0,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00035_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00035_05_dot.png "drawn by dot")

## Graph 36

```
0,1,1,0,0,1,1
1,0,1,1,1,1,1
1,1,0,0,1,1,1
0,1,0,0,0,1,1
0,1,1,0,0,0,1
1,1,1,1,0,0,1
1,1,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00036_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00036_05_dot.png "drawn by dot")

## Graph 37

```
0,1,1,1,1,1,1
1,0,0,0,1,1,1
1,0,0,1,1,1,1
1,0,1,0,1,1,1
1,1,1,1,0,1,0
1,1,1,1,1,0,0
1,1,1,1,0,0,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00037_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00037_05_dot.png "drawn by dot")

## Graph 38

```
0,1,0,0,1,1,1
1,0,1,1,1,0,1
0,1,0,1,1,1,1
0,1,1,0,1,0,1
1,1,1,1,0,1,1
1,0,1,0,1,0,1
1,1,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00038_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00038_05_dot.png "drawn by dot")

## Graph 39

```
0,1,0,1,1,0,1
1,0,1,1,1,1,1
0,1,0,1,1,1,1
1,1,1,0,1,1,1
1,1,1,1,0,0,0
0,1,1,1,0,0,1
1,1,1,1,0,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00039_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00039_05_dot.png "drawn by dot")

## Graph 40

```
0,1,1,1,1,1,1
1,0,1,1,0,0,0
1,1,0,1,1,1,1
1,1,1,0,0,1,1
1,0,1,0,0,1,1
1,0,1,1,1,0,1
1,0,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00040_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00040_05_dot.png "drawn by dot")

## Graph 41

```
0,1,1,0,1,1,1
1,0,0,1,1,1,1
1,0,0,1,1,1,1
0,1,1,0,0,1,0
1,1,1,0,0,1,1
1,1,1,1,1,0,1
1,1,1,0,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00041_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00041_05_dot.png "drawn by dot")

## Graph 42

```
0,1,1,1,1,0,0
1,0,0,0,1,1,1
1,0,0,1,1,1,1
1,0,1,0,1,1,1
1,1,1,1,0,1,1
0,1,1,1,1,0,1
0,1,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00042_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00042_05_dot.png "drawn by dot")

## Graph 43

```
0,0,1,1,1,1,1
0,0,0,1,1,1,0
1,0,0,0,1,1,1
1,1,0,0,1,1,1
1,1,1,1,0,1,1
1,1,1,1,1,0,1
1,0,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00043_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00043_05_dot.png "drawn by dot")

## Graph 44

```
0,1,1,1,1,1,1
1,0,1,1,1,1,1
1,1,0,1,1,1,1
1,1,1,0,1,0,1
1,1,1,1,0,0,0
1,1,1,0,0,0,1
1,1,1,1,0,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00044_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00044_05_dot.png "drawn by dot")

## Graph 45

```
0,1,1,0,1,1,1
1,0,1,1,1,1,0
1,1,0,1,1,1,1
0,1,1,0,1,1,1
1,1,1,1,0,1,1
1,1,1,1,1,0,1
1,0,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00045_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00045_05_dot.png "drawn by dot")

## Graph 46

```
0,0,1,1,1,1,1
0,0,1,1,1,1,1
1,1,0,1,1,1,1
1,1,1,0,1,0,1
1,1,1,1,0,1,1
1,1,1,0,1,0,1
1,1,1,1,1,1,0
```

Its chromatic number is 5.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00046_05_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00046_05_dot.png "drawn by dot")

## Graph 47

```
0,1,1,1,1,1,1
1,0,0,1,1,1,1
1,0,0,1,1,0,1
1,1,1,0,1,1,1
1,1,1,1,0,1,1
1,1,0,1,1,0,1
1,1,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00047_06_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00047_06_dot.png "drawn by dot")

## Graph 48

```
0,1,1,1,1,1,1
1,0,1,1,1,1,1
1,1,0,1,1,1,1
1,1,1,0,0,1,0
1,1,1,0,0,1,1
1,1,1,1,1,0,1
1,1,1,0,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00048_06_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00048_06_dot.png "drawn by dot")

## Graph 49

```
0,1,1,1,1,1,1
1,0,1,1,1,1,1
1,1,0,1,1,1,1
1,1,1,0,1,0,1
1,1,1,1,0,1,1
1,1,1,0,1,0,1
1,1,1,1,1,1,0
```

Its chromatic number is 6.

The colored graph as drawn by circo and dot:

![drawn by circo](png/random_07,0.75_00049_06_circo.png "drawn by circo")

![drawn by dot](png/random_07,0.75_00049_06_dot.png "drawn by dot")

