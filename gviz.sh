#! /bin/sh

set -u

cd dot || exit 1

for f in *.dot; do
	g="$(basename "$f" .dot)"
	circo -Tpng -o ../png/"$g"_circo.png "$f"
	dot   -Tpng -o ../png/"$g"_dot.png "$f"
done
